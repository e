/*
 * vim:ts=8:sw=3:sts=8:noexpandtab:cino=>5n-3f0^-2{2
 */
#ifdef E_TYPEDEFS
#else
#ifndef E_INT_SHELF_CONFIG_H
#define E_INT_SHELF_CONFIG_H

EAPI void e_int_shelf_config(E_Shelf *es);

#endif
#endif
