#ifndef E_WIDGET_COLOR_WELL_H
#define E_WIDGET_COLOR_WELL_H

EAPI Evas_Object *e_widget_color_well_add    (Evas *evas, E_Color *color, int show_color_dialog);
EAPI void         e_widget_color_well_update (Evas_Object *obj);

#endif
