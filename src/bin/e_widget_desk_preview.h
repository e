#ifndef E_WIDGET_DESK_PREVIEW_H
#define E_WIDGET_DESK_PREVIEW_H

EAPI Evas_Object *e_widget_desk_preview_add(Evas *evas, int nx, int ny);
EAPI void e_widget_desk_preview_num_desks_set(Evas_Object *obj, int nx, int ny);

#endif
