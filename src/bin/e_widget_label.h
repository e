/*
 * vim:ts=8:sw=3:sts=8:noexpandtab:cino=>5n-3f0^-2{2
 */
#ifdef E_TYPEDEFS
#else
#ifndef E_WIDGET_LABEL_H
#define E_WIDGET_LABEL_H

EAPI Evas_Object *e_widget_label_add(Evas *evas, const char *label);
EAPI void e_widget_label_text_set(Evas_Object *obj, const char *text);

#endif
#endif
