/*
 * vim:ts=8:sw=3:sts=8:noexpandtab:cino=>5n-3f0^-2{2
 */
#ifdef E_TYPEDEFS
#else
#ifndef E_WIDGET_LIST_H
#define E_WIDGET_LIST_H

EAPI Evas_Object *e_widget_list_add(Evas *evas, int homogenous, int horiz);
EAPI void e_widget_list_object_append(Evas_Object *obj, Evas_Object *sobj, int fill, int expand, double align);
    

#endif
#endif
