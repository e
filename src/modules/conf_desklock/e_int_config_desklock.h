#ifdef E_TYPEDEFS
#else
#ifndef E_INT_CONFIG_DESKLOCK_H
#define E_INT_CONFIG_DESKLOCK_H

EAPI E_Config_Dialog *e_int_config_desklock(E_Container *con, const char *params __UNUSED__);

#endif
#endif
