#ifdef E_TYPEDEFS
#else
#ifndef E_INT_CONFIG_THEME_IMPORT_H
#define E_INT_CONFIG_THEME_IMPORT_H

EAPI E_Win *e_int_config_theme_import (E_Config_Dialog *parent);
EAPI void   e_int_config_theme_del    (E_Win *win);

#endif
#endif
